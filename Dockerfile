from debian:stretch-slim
MAINTAINER team@f-droid.org

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive \
    ANDROID_HOME=/opt/android-sdk \
    GRADLE_USER_HOME=$HOME/.gradle \
    GRADLE_OPTS=-Dorg.gradle.daemon=false

RUN echo Etc/UTC > /etc/timezone \
	&& echo 'APT::Install-Recommends "0";' \
		'APT::Install-Suggests "0";' \
		'APT::Acquire::Retries "20";' \
		'APT::Get::Assume-Yes "true";' \
		'Dpkg::Use-Pty "0";' \
		'quiet "1";' \
        >> /etc/apt/apt.conf.d/99gitlab

RUN printf "Package: androguard fdroidserver python3-asn1crypto python3-ruamel.yaml yamllint\nPin: release a=stretch-backports\nPin-Priority: 500\n" > /etc/apt/preferences.d/debian-stretch-backports.pref \
	&& echo "deb http://deb.debian.org/debian/ stretch-backports main" > /etc/apt/sources.list.d/backports.list

# Misc tools
# Python 3 (fdroidserver, fdroidclient/tools)
# Deps for `fdroid lint` in fdroiddata
# OpenJDK 8
# Android SDK with common components
RUN printf "path-exclude=/usr/share/locale/*\npath-exclude=/usr/share/man/*\npath-exclude=/usr/share/doc/*\npath-include=/usr/share/doc/*/copyright\n" >/etc/dpkg/dpkg.cfg.d/01_nodoc \
	&& mkdir -p /usr/share/man/man1 \
	&& apt-get update \
	&& apt-get upgrade \
	&& apt-get dist-upgrade \
	&& apt-get install \
		androguard \
		curl \
		default-jdk-headless \
		gcc \
		git \
		gnupg \
		lib32stdc++6 \
		lib32z1 \
		libffi-dev \
		libjpeg-dev \
		libssl-dev \
		make \
		python3-asn1crypto \
		python3-babel \
		python3-defusedxml \
		python3-dev \
		python3-pip \
		python3-ruamel.yaml \
		python3-setuptools \
		python3-venv \
		rsync \
		ruby \
		unzip \
		wget \
		zlib1g-dev \
		`apt-cache depends fdroidserver | grep -Fv -e java -e jdk -e '<' | awk '/Depends:/{print$2}'` \
	&& apt-get autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

# Install SDK components one at a time, so that `echo y` applies to
# each license There is also a new kind of insane license accept logic
# that happens at build time.  If we pre-load the license hashes of
# the licenses that we support, then Gradle Android Plugin should no
# longer prompt for things with that license when it is installing
# them. It is a hack. See:
# https://code.google.com/p/android/issues/detail?id=212128#c17
# https://hub.docker.com/r/iluretar/gitlab-ci-android/~/dockerfile
RUN wget --quiet -O tools.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip \
	&& echo "92ffee5a1d98d856634e8b71132e8a95d96c83a63fde1099be3d86df3106def9  tools.zip" > tools.zip.sha256 \
	&& sha256sum -c tools.zip.sha256 \
	&& unzip tools.zip \
	&& rm tools.zip* \
	&& mkdir $ANDROID_HOME \
	&& mv tools $ANDROID_HOME/ \
	&& grep -v '^License' $ANDROID_HOME/tools/source.properties \
	&& mkdir -p $ANDROID_HOME/licenses/ \
	&& printf "\n8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e" > $ANDROID_HOME/licenses/android-sdk-license \
	&& printf "\n84831b9409646a918e30573bab4c9c91346d8abd" > $ANDROID_HOME/licenses/android-sdk-preview-license \
	&& printf "\n79120722343a6f314e0719f863036c702b0e6b2a\n84831b9409646a918e30573bab4c9c91346d8abd" > $ANDROID_HOME/licenses/android-sdk-preview-license-old \
	&& mkdir /root/.android \
	&& touch /root/.android/repositories.cfg \
	&& echo y | $ANDROID_HOME/tools/bin/sdkmanager "platform-tools" \
	&& echo y | $ANDROID_HOME/tools/bin/sdkmanager "build-tools;27.0.3" \
	&& echo y | $ANDROID_HOME/tools/bin/sdkmanager --update

COPY test /

# disable gradle daemon, it is to speed up repeat builds
RUN mkdir -p $GRADLE_USER_HOME && echo "org.gradle.daemon=false" >> $GRADLE_USER_HOME/gradle.properties
